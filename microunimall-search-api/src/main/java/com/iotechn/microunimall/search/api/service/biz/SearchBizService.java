package com.iotechn.microunimall.search.api.service.biz;

import com.iotechn.microunimall.core.model.Page;
import com.iotechn.microunimall.shopping.api.dto.SpuDTO;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/19
 * Time: 0:34
 */
public interface SearchBizService {

    public List<String> hotKeys();

    public Page<SpuDTO> searchSpu(String keyWords, Integer page, Integer pageSize);

}
