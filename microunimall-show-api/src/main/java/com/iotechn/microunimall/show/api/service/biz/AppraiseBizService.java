package com.iotechn.microunimall.show.api.service.biz;

import com.iotechn.microunimall.core.exception.ServiceException;
import com.iotechn.microunimall.core.model.Page;
import com.iotechn.microunimall.show.api.dto.AppraiseRequestDTO;
import com.iotechn.microunimall.show.api.dto.AppraiseDTO;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/10
 * Time: 16:13
 */
public interface AppraiseBizService {

    public Page<AppraiseDTO> getSpuAllAppraise(Long spuId, Integer pageNo, Integer pageSize) throws ServiceException;

    public String addAppraise(AppraiseRequestDTO appraiseRequestDTO) throws ServiceException;

}
