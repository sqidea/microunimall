package com.iotechn.microunimall.show.api.domain;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/*
@author kbq
@date  2019/7/6 - 10:14
*/
@TableName("unimall_appraise")
@Data
public class AppraiseDO extends SuperDO implements Serializable {

    @TableField("spu_id")
    private Long spuId;

    @TableField("sku_id")
    private Long skuId;

    @TableField("order_id")
    private Long orderId;

    @TableField("user_id")
    private Long userId;

    /**
     * 评论内容
     */
    private String content;

    /**
     * 评论星数
     */
    private Integer score;

    @TableField("sku_title")
    private String skuTitle;

    @TableField("user_nick_name")
    private String userNickName;

    @TableField("user_avatar_url")
    private String userAvatarUrl;


}
