package com.iotechn.microunimall.order.api.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by rize on 2019/7/6.
 */
@Data
public class OrderRequestSkuDTO implements Serializable {

    private Long skuId;

    private Integer price;

    private Integer num;

}
