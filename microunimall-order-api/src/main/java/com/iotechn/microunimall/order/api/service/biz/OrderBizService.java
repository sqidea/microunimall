package com.iotechn.microunimall.order.api.service.biz;

import com.iotechn.microunimall.core.exception.ServiceException;
import com.iotechn.microunimall.order.api.domain.OrderDO;
import com.iotechn.microunimall.order.api.dto.OrderDTO;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/11
 * Time: 22:23
 */
public interface OrderBizService {

    public boolean changeOrderStatus(String orderNo, int nowStatus, OrderDO orderDO) throws ServiceException;

    public OrderDO checkOrderExist(String orderNo, Long userId) throws ServiceException;

    public OrderDTO getOrderDetail(Long orderId, Long userId) throws ServiceException;

}
