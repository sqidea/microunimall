package com.iotechn.microunimall.user.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Created by rize on 2019/5/20.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WeChatTemplateValueModel implements Serializable {

    public WeChatTemplateValueModel(String value) {
        this.value = value;
    }

    private String value;

    private String color;

}
