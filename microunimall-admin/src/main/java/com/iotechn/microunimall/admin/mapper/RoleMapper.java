package com.iotechn.microunimall.admin.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.iotechn.microunimall.admin.api.domain.RoleDO;

/**
 * Created by rize on 2019/7/8.
 */
public interface RoleMapper extends BaseMapper<RoleDO> {
}
