package com.iotechn.microunimall.admin;

import com.iotechn.microunimall.core.annotation.EnableUnimallSMSClient;
import com.iotechn.microunimall.data.starter.annotaion.EnableUnimallDefaultMysql;
import com.iotechn.microunimall.data.starter.annotaion.EnableUnimallDefaultRedis;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisReactiveAutoConfiguration;
import org.springframework.context.annotation.ImportResource;

@ImportResource("dubbo.xml")
@EnableUnimallDefaultMysql
@EnableUnimallDefaultRedis
@EnableUnimallSMSClient
@MapperScan({"com.iotechn.microunimall.admin.mapper*"})
@SpringBootApplication(scanBasePackages = "com.iotechn.microunimall.admin", exclude = {RedisAutoConfiguration.class, RedisReactiveAutoConfiguration.class})
public class MicrounimallAdminApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(MicrounimallAdminApplication.class, args);
    }

}
