package com.iotechn.microunimall.core.annotation;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/15
 * Time: 11:11
 */
public enum  RateLimitType {

    NONE,
    USER,
    IP,
    ALL

}
