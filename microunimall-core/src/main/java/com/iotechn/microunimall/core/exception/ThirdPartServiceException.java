package com.iotechn.microunimall.core.exception;

import java.io.Serializable;

/**
 * 第三方接口服务异常
 * Created by rize on 2019/7/3.
 */
public class ThirdPartServiceException extends ServiceException implements Serializable {

    public ThirdPartServiceException() {}

    public ThirdPartServiceException(String message, int code) {
        super(message, code);
    }

}
