package com.iotechn.microunimall.core.config;

import com.iotechn.microunimall.core.notify.AliyunSMSClient;
import com.iotechn.microunimall.core.notify.MockSMSClient;
import com.iotechn.microunimall.core.notify.QCloudSMSClient;
import com.iotechn.microunimall.core.notify.SMSClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/9
 * Time: 16:04
 */
@Configuration
public class SMSConfiguration {

    @Value("${sms.enable}")
    private String enable;

    @Bean
    public SMSClient smsClient() {
        if ("qcloud".equals(enable)) {
            return new QCloudSMSClient();
        } else if ("aliyun".equals(enable)) {
            return new AliyunSMSClient();
        } else if ("mock".equals(enable)) {
            return new MockSMSClient();
        } else {
            return new MockSMSClient();
        }
    }

}
