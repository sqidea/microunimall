package com.iotechn.microunimall.core.exception;

import java.io.Serializable;

/**
 * Created by rize on 2019/7/1.
 */
public class AppServiceException extends ServiceException implements Serializable {

    public AppServiceException() {}

    public AppServiceException(ServiceExceptionDefinition definition) {
        super(definition);
    }

    public AppServiceException(String message, int code) {
        super(message,code);
    }
}
