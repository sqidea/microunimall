package com.iotechn.microunimall.core.annotation;

import com.iotechn.microunimall.core.config.SMSConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/9
 * Time: 16:06
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Import(SMSConfiguration.class)
@Documented
public @interface EnableUnimallSMSClient {
}
