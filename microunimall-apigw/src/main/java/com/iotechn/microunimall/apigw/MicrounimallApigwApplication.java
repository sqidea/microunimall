package com.iotechn.microunimall.apigw;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@ImportResource("classpath:dubbo.xml")
@SpringBootApplication
public class MicrounimallApigwApplication {

    private static final Logger logger = LoggerFactory.getLogger(MicrounimallApigwApplication.class);

    public static void main(String[] args) throws Exception {
        SpringApplication.run(MicrounimallApigwApplication.class, args);
        logger.info("[Api网关初始化完成]");
    }

}
