package com.iotechn.microunimall.show.service.biz;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.iotechn.microunimall.core.Const;
import com.iotechn.microunimall.core.enums.BizType;
import com.iotechn.microunimall.core.exception.ServiceException;
import com.iotechn.microunimall.core.model.Page;
import com.iotechn.microunimall.data.starter.compent.CacheComponent;
import com.iotechn.microunimall.show.api.domain.AppraiseDO;
import com.iotechn.microunimall.show.api.domain.ImgDO;
import com.iotechn.microunimall.show.api.dto.AppraiseRequestDTO;
import com.iotechn.microunimall.show.api.dto.AppraiseRequestItemDTO;
import com.iotechn.microunimall.show.api.dto.AppraiseDTO;
import com.iotechn.microunimall.show.api.service.biz.AppraiseBizService;
import com.iotechn.microunimall.show.mapper.AppraiseMapper;
import com.iotechn.microunimall.show.mapper.ImgMapper;
import com.iotechn.microunimall.user.api.dto.UserDTO;
import com.iotechn.microunimall.user.api.util.SessionUtil;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/13
 * Time: 15:45
 */
@Service("appraiseBizService")
public class AppraiseBizServiceImpl implements AppraiseBizService {

    @Autowired
    private AppraiseMapper appraiseMapper;
    @Autowired
    private CacheComponent cacheComponent;
    @Autowired
    private ImgMapper imgMapper;

    public static final String CA_APPRAISE_KEY = "CA_APPRAISE_";

    public Page<AppraiseDTO> getSpuAllAppraise(Long spuId, Integer pageNo, Integer pageSize) throws ServiceException {
        String cacheKey = CA_APPRAISE_KEY + spuId + "_" + pageNo + "_" + pageSize;
        Page obj = cacheComponent.getObj(cacheKey, Page.class);
        if (obj != null) {
            return obj;
        }
        List<AppraiseDTO> appraiseDTOList = appraiseMapper.selectPage(new RowBounds((pageNo - 1) * pageSize, pageSize),
                new EntityWrapper<AppraiseDO>()
                        .eq("spu_id", spuId)
                        .orderBy("id", false))
                .stream()
                .map(item -> {
                    AppraiseDTO appraiseDTO = new AppraiseDTO();
                    BeanUtils.copyProperties(item, appraiseDTO);
                    appraiseDTO.setImgList(imgMapper.getImgs(BizType.COMMENT.getCode(), item.getId()));
                    return appraiseDTO;
                }).collect(Collectors.toList());
        Integer count = appraiseMapper.selectCount(
                new EntityWrapper<AppraiseDO>()
                        .eq("spu_id", spuId));
        Page<AppraiseDTO> page = new Page<>(appraiseDTOList, pageNo, pageSize, count);
        cacheComponent.putObj(cacheKey, page, Const.CACHE_ONE_DAY);
        return page;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String addAppraise(AppraiseRequestDTO appraiseRequestDTO) throws ServiceException {
        //循环读取订单评价中所有商品的评价
        Date now = new Date();
        UserDTO user = SessionUtil.getUser();
        for (AppraiseRequestItemDTO appraiseDTO : appraiseRequestDTO.getAppraiseDTOList()) {
            //从order_sku表中 验证是否有对应的表单和商品
            AppraiseDO appraiseDO = new AppraiseDO();
            BeanUtils.copyProperties(appraiseDTO, appraiseDO);
            appraiseDO.setSpuId(appraiseDTO.getSpuId());
            appraiseDO.setId(null); //防止传入id,导致插入数据库出错
            appraiseDO.setOrderId(appraiseRequestDTO.getOrderId()); //从传入数据取出，不使用DTO中的冗余数据
            appraiseDO.setUserId(user.getId());
            appraiseDO.setSkuTitle(appraiseDTO.getSkuTitle());
            appraiseDO.setUserNickName(user.getNickname());
            appraiseDO.setUserAvatarUrl(user.getAvatarUrl());
            appraiseDO.setGmtCreate(now);
            appraiseDO.setGmtUpdate(appraiseDO.getGmtCreate());
            appraiseMapper.insert(appraiseDO);  //插入该订单该商品评价
            cacheComponent.delPrefixKey(AppraiseBizServiceImpl.CA_APPRAISE_KEY + appraiseDO.getSpuId()); //删除商品评论缓存
            if (appraiseDTO.getImgUrl() == null || appraiseDTO.getImgUrl().equals("")) {
                continue;
            }
            String imgUrlS = appraiseDTO.getImgUrl();
            String[] imgUrlList = imgUrlS.split(",");   //传入图片
            for (String imgurl : imgUrlList) {
                ImgDO imgDO = new ImgDO();
                imgDO.setBizType(BizType.COMMENT.getCode());
                imgDO.setBizId(appraiseDO.getId());
                imgDO.setUrl(imgurl);
                imgDO.setGmtCreate(now);
                imgDO.setGmtUpdate(imgDO.getGmtCreate());
                imgMapper.insert(imgDO);
            }
        }
        return "ok";
    }

}
