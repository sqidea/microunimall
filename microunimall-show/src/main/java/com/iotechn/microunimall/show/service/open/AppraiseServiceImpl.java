package com.iotechn.microunimall.show.service.open;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.iotechn.microunimall.core.enums.BizType;
import com.iotechn.microunimall.core.exception.AppServiceException;
import com.iotechn.microunimall.core.exception.ExceptionDefinition;
import com.iotechn.microunimall.core.exception.ServiceException;
import com.iotechn.microunimall.core.model.Page;
import com.iotechn.microunimall.show.api.domain.AppraiseDO;
import com.iotechn.microunimall.show.api.dto.AppraiseDTO;
import com.iotechn.microunimall.show.api.service.biz.AppraiseBizService;
import com.iotechn.microunimall.show.api.service.open.AppraiseService;
import com.iotechn.microunimall.show.mapper.AppraiseMapper;
import com.iotechn.microunimall.show.mapper.ImgMapper;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/13
 * Time: 14:25
 */
@Service("appraiseService")
public class AppraiseServiceImpl implements AppraiseService {

    @Autowired
    private ImgMapper imgMapper;
    @Autowired
    private AppraiseMapper appraiseMapper;
    @Autowired
    private AppraiseBizService appraiseBizService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean deleteAppraiseById(Long appraiseId, Long userId) throws ServiceException {
        Integer delete = appraiseMapper.delete(
                new EntityWrapper<AppraiseDO>()
                        .eq("id", appraiseId)
                        .eq("user_id", userId));
        if (delete > 0) {
            return true;
        } else {
            throw new AppServiceException(ExceptionDefinition.APPRAISE_PARAM_CHECK_FAILED);
        }
    }

    @Override
    public Page<AppraiseDTO> getUserAllAppraise(Long userId, Integer pageNo, Integer pageSize) throws ServiceException {
        Wrapper<AppraiseDO> wrapper = new EntityWrapper<AppraiseDO>().eq("user_id", userId);
        Integer count = appraiseMapper.selectCount(wrapper);
        List<AppraiseDTO> appraiseDTOS = appraiseMapper.selectPage(new RowBounds((pageNo - 1) * pageSize, pageSize), wrapper)
                .stream()
                .map(item -> {
                    AppraiseDTO appraiseDTO = new AppraiseDTO();
                    BeanUtils.copyProperties(item, appraiseDTO);
                    appraiseDTO.setImgList(imgMapper.getImgs(BizType.COMMENT.getCode(), item.getId()));
                    return appraiseDTO;
                }).collect(Collectors.toList());
        Page<AppraiseDTO> page = new Page<>(appraiseDTOS, pageNo, pageSize, count);
        return page;
    }

    @Override
    public Page<AppraiseDTO> getSpuAllAppraise(Long spuId, Integer pageNo, Integer pageSize) throws ServiceException {
        return appraiseBizService.getSpuAllAppraise(spuId, pageNo, pageSize);
    }

    @Override
    public AppraiseDTO getOneById(Long userId, Long appraiseId) throws ServiceException {
        AppraiseDO appraiseDO = appraiseMapper.selectById(appraiseId);
        AppraiseDTO appraiseDTO = new AppraiseDTO();
        if (appraiseDO == null) {
            throw new AppServiceException(ExceptionDefinition.APPRAISE_PARAM_CHECK_FAILED);
        }
        BeanUtils.copyProperties(appraiseDO, appraiseDTO);
        appraiseDTO.setImgList(imgMapper.getImgs(BizType.COMMENT.getCode(), appraiseDO.getId()));
        return appraiseDTO;
    }

}
