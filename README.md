> microunimall 是unimall的微服务版，接口与unimall 98% 一致，使用微服务版无需额外进行前端开发。microunimall采用SpringBoot、Dubbo等易用框架，提供限流网关、分布式事务等。 unimall使用uniapp前端框架，可同时编译到 微信小程序、H5、Android App、iOS App等几个平台，可为中小商户企业节约大量维护成本。也可支撑中小商户企业前期平台横扩需求。

---

##### QQ讨论群：656676341 (进群前，请在网页右上角点star)

优先更新地址：[https://gitee.com/iotechn/microunimall](https://gitee.com/iotechn/microunimall)

集群版地址：[https://gitee.com/iotechn/unimall](https://gitee.com/iotechn/unimall) 减少部署机器

---

#### 技术总览

| 技术 | 选型 |
| --- | --- |
| IoC、Aop | SpringBoot |
| 持久层 | MyBatis-Plus + MySQL |
| 缓存、锁 | Redis |
| 路由网关 | 自编写 |
| 限流框架 | Sentinel |
| 分布式事务 | Seata |
| RPC | Dubbo |
| 服务降级 | Dubbo Mock 或 Seata |
| 注册中心 | Nacos |
| 动态配置 | - TODO - |
| 搜索引擎 | - TODO - |

| 技术 | 选型 |
| --- | --- |
| 前台前端 | uniapp |
| 后台前端 | vuejs + elementui |

#### 服务总览

> 服务从下到上。DOING 表示可用，但还有计划的功能没完成！


| 服务 | 职责 | 完成 |
| --- | --- | --- |
| 搜索服务 | 商品搜索 | TODO |
| 用户服务 | 用户登录、注册等 | DONE |
| 费用服务 | 优惠券、运费、其他活动 | DOING |
| 展示服务 | 买家评论、买家秀 | DOING |
| 商品服务 | 商品浏览、购买、收藏、购物车等 | DONE |
| 订单服务 | 订单浏览、创建、支付等 | DONE |
| 广告服务 | 前台界面内容管理 | DOING |
| 管理服务 | 用户、商品、订单等后台管理 | TODO |
| API网关 | 路由转发、权限校验、网关限流 | DONE |

#### 架构总览

![架构总览](snapshoot/framework.png)

#### 接口变更

> unimall 过度到 microunimall 时，有少许接口变更。需要前端适配

appraise.addAppraise -> order.addAppraise (名称变更)

freight.getFreightMoney -> order.preview (名称变更)

#### 前端展示

![前端演示](snapshoot/front.jpg)

#### 新注解使用

> 如果不知道HttpOpenApi等注解，请先参考[unimall](https://gitee.com/iotechn/unimall)项目中README的二次开发文档。

    @HttpMethod(description = "发送验证码到用户手机", rateLimit = RateLimitType.IP, rateQps = 10, rateLimitDurationInSec = 60)
    
   
  HttpMethod 中加入网关限流功能。
  
  rateLimit 表示限流类型、支持全部限流、用户限流(需要用户登录)、IP限流三种。
  
  rateQps 表示单位时间内，可以允许多少流量。
  
  rateLimitDurationInSec 表示时间窗口宽度
  
  综上所诉的此方法允许六十秒内 同IP允许访问10次。
  
    @EnableUnimallDefaultMysql
    @EnableUnimallDefaultRedis
    @EnableUnimallSeataGlobalTransaction
    public class MicrounimallOrderApplication {
        ...
        
 微服务技术选型按道理是各小组内部决策选型。但是由于所有系统都是一个人写的，所以选型都一样。 所以microunimall提供默认的DB、Redis策略。@EnableUnimallDefaultMysql 和 @EnableUnimallDefaultRedis 注解后，将采用默认的数据库缓存策略。
 
  @EnableUnimallSeataGlobalTransaction 表示该服务加入到分布式事务中。可通过Seata的注解
  
    io.seata.spring.annotation.GlobalTransactional

开启全局事务。


#### 版权声明

本项目有重庆驽驹科技有限公司开发，禁止未经授权用于商业用途。

本项目有重庆驽驹科技有限公司开发，禁止未经授权用于商业用途。

本项目有重庆驽驹科技有限公司开发，禁止未经授权用于商业用途。