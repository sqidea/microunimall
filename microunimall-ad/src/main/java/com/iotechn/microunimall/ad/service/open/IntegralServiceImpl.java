package com.iotechn.microunimall.ad.service.open;

import com.iotechn.microunimall.ad.api.domain.AdvertisementDO;
import com.iotechn.microunimall.ad.api.dto.AdvertisementDTO;
import com.iotechn.microunimall.ad.api.dto.IntegralIndexDataDTO;
import com.iotechn.microunimall.ad.api.enums.AdvertisementType;
import com.iotechn.microunimall.ad.api.enums.RecommendType;
import com.iotechn.microunimall.ad.api.service.open.AdvertisementService;
import com.iotechn.microunimall.ad.api.service.open.IntegralService;
import com.iotechn.microunimall.core.exception.ServiceException;
import com.iotechn.microunimall.core.model.Page;
import com.iotechn.microunimall.shopping.api.dto.RecommendDTO;
import com.iotechn.microunimall.shopping.api.dto.SpuDTO;
import com.iotechn.microunimall.shopping.api.service.biz.GoodsBizService;
import com.iotechn.microunimall.shopping.api.service.biz.RecommendBizService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/17
 * Time: 16:26
 */
@Service("integralService")
public class IntegralServiceImpl implements IntegralService {

    @Autowired
    private AdvertisementService advertisementService;

    @Autowired
    private GoodsBizService goodsBizService;

    @Autowired
    private RecommendBizService recommendBizService;

    @Override
    public IntegralIndexDataDTO getIndexData() throws ServiceException {
        //分类
        List<AdvertisementDO> activeAd = advertisementService.getActiveAd(null);
        Map<String, List<AdvertisementDTO>> adDTOMap = activeAd.stream().map(item -> {
            AdvertisementDTO advertisementDTO = new AdvertisementDTO();
            BeanUtils.copyProperties(item, advertisementDTO);
            return advertisementDTO;
        }).collect(Collectors.groupingBy(item -> "t" + item.getAdType()));
        List<AdvertisementDTO> categoryPickAd = adDTOMap.get("t" + AdvertisementType.CATEGORY_PICK.getCode());
        //封装 分类精选 商品
        if (!CollectionUtils.isEmpty(categoryPickAd)) {
            for (AdvertisementDTO item : categoryPickAd) {
                Page<SpuDTO> pickPage = goodsBizService.getGoodsPage(1, 10, new Long(item.getUrl().substring(item.getUrl().lastIndexOf("=") + 1)), "sales", false,null);
                item.setData(pickPage.getItems());
            }
        }
        IntegralIndexDataDTO integralIndexDataDTO = new IntegralIndexDataDTO();
        integralIndexDataDTO.setAdvertisement(adDTOMap);

        /**
         * 橱窗推荐
         */
        List<RecommendDTO> windowRecommend = recommendBizService.getRecommendByType(RecommendType.WINDOW.getCode(), 1, 10);
        integralIndexDataDTO.setWindowRecommend(windowRecommend);

        /**
         * 销量冠军
         */
        List<SpuDTO> salesTop = goodsBizService.getGoodsPage(1, 8, null, "sales", false, null).getItems();
        integralIndexDataDTO.setSalesTop(salesTop);

        /**
         * 最近上新
         */
        List<SpuDTO> newTop = goodsBizService.getGoodsPage(1, 8, null, "id", false, null).getItems();
        integralIndexDataDTO.setNewTop(newTop);
        return integralIndexDataDTO;
    }

}
