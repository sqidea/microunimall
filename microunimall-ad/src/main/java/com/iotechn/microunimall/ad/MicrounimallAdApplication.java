package com.iotechn.microunimall.ad;

import com.iotechn.microunimall.data.starter.annotaion.EnableUnimallDefaultMysql;
import com.iotechn.microunimall.data.starter.annotaion.EnableUnimallDefaultRedis;
import com.iotechn.microunimall.data.starter.annotaion.EnableUnimallSeataGlobalTransaction;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisReactiveAutoConfiguration;
import org.springframework.context.annotation.ImportResource;

@ImportResource("classpath:dubbo.xml")
@MapperScan({"com.iotechn.microunimall.ad.mapper*"})
@EnableUnimallDefaultMysql
@EnableUnimallDefaultRedis
@EnableUnimallSeataGlobalTransaction
@SpringBootApplication(exclude = {RedisAutoConfiguration.class, RedisReactiveAutoConfiguration.class})
public class MicrounimallAdApplication {

    private static final Logger logger = LoggerFactory.getLogger(MicrounimallAdApplication.class);

    public static void main(String[] args) throws Exception {
        SpringApplication.run(MicrounimallAdApplication.class, args);
        logger.info("[广告服务初始化完成]");
    }

}
