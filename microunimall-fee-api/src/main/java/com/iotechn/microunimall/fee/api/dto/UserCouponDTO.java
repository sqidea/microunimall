package com.iotechn.microunimall.fee.api.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by rize on 2019/7/5.
 */
@Data
public class UserCouponDTO extends SuperDTO implements Serializable {

    private String title;

    private String range;

    private String categoryTitle;

    private Integer min;

    /**
     * 优惠券价格
     */
    private Integer discount;

    private Long userId;

    private Long couponId;

    private Long orderId;

    private Date gmtUsed;

    private Date gmtStart;

    private Date gmtEnd;

}
