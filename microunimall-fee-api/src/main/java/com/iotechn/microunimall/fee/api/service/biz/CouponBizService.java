package com.iotechn.microunimall.fee.api.service.biz;

import com.iotechn.microunimall.fee.api.domain.UserCouponDO;
import com.iotechn.microunimall.fee.api.dto.UserCouponDTO;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/13
 * Time: 13:01
 */
public interface CouponBizService {

    public UserCouponDTO getUserCouponById(Long userCouponId, Long userId);

    public Integer updateById(UserCouponDO userCouponDO);

}
