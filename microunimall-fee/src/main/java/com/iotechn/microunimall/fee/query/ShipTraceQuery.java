package com.iotechn.microunimall.fee.query;


import com.iotechn.microunimall.core.exception.ServiceException;
import com.iotechn.microunimall.fee.api.dto.ShipTraceDTO;

/**
 * Created by rize on 2019/7/10.
 */
public interface ShipTraceQuery {

    public ShipTraceDTO query(String shipNo, String shipCode) throws ServiceException;

}
