package com.iotechn.microunimall.fee.service.open;

import com.iotechn.microunimall.core.exception.AppServiceException;
import com.iotechn.microunimall.core.exception.ExceptionDefinition;
import com.iotechn.microunimall.core.exception.ServiceException;
import com.iotechn.microunimall.fee.api.service.biz.FreightBizService;
import com.iotechn.microunimall.fee.api.service.open.FreightTemplateService;
import com.iotechn.microunimall.user.api.service.biz.AddressBizService;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: kbq
 * Date: 2019-07-07
 * Time: 下午7:50
 */
@Service("freightTemplateService")
public class FreightTemplateServiceImpl implements FreightTemplateService {

    @Autowired
    private AddressBizService addressBizService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    @GlobalTransactional
    public Integer testSeata() throws ServiceException {
        addressBizService.create();
        throw new AppServiceException(ExceptionDefinition.THIRD_PART_SERVICE_EXCEPTION);
    }

}
