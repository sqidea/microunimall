package com.iotechn.microunimall.order.service.open;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.binarywang.wxpay.bean.order.WxPayMpOrderResult;
import com.github.binarywang.wxpay.bean.request.WxPayUnifiedOrderRequest;
import com.github.binarywang.wxpay.constant.WxPayConstants;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.github.binarywang.wxpay.service.WxPayService;
import com.iotechn.microunimall.core.exception.AppServiceException;
import com.iotechn.microunimall.core.exception.ExceptionDefinition;
import com.iotechn.microunimall.core.exception.ServiceException;
import com.iotechn.microunimall.core.exception.ThirdPartServiceException;
import com.iotechn.microunimall.core.model.Page;
import com.iotechn.microunimall.core.util.GeneratorUtil;
import com.iotechn.microunimall.data.starter.compent.LockComponent;
import com.iotechn.microunimall.data.starter.config.UnimallAppConfiguration;
import com.iotechn.microunimall.fee.api.domain.UserCouponDO;
import com.iotechn.microunimall.fee.api.dto.FreightCalcRequest;
import com.iotechn.microunimall.order.api.dto.*;
import com.iotechn.microunimall.shopping.api.domain.GoodsStrategyDO;
import com.iotechn.microunimall.shopping.api.enums.GoodsStrategyType;
import com.iotechn.microunimall.shopping.api.enums.GoodsStrategyUnionType;
import com.iotechn.microunimall.fee.api.dto.ShipTraceDTO;
import com.iotechn.microunimall.fee.api.dto.UserCouponDTO;
import com.iotechn.microunimall.fee.api.service.biz.CouponBizService;
import com.iotechn.microunimall.fee.api.service.biz.FreightBizService;
import com.iotechn.microunimall.shopping.api.dto.SkuDTO;
import com.iotechn.microunimall.shopping.api.service.biz.CartBizService;
import com.iotechn.microunimall.shopping.api.service.biz.CategoryBizService;
import com.iotechn.microunimall.shopping.api.service.biz.GoodsBizService;
import com.iotechn.microunimall.order.api.domain.OrderDO;
import com.iotechn.microunimall.order.api.domain.OrderSkuDO;
import com.iotechn.microunimall.order.api.enums.OrderStatusType;
import com.iotechn.microunimall.order.api.enums.PayChannelType;
import com.iotechn.microunimall.order.api.enums.UserLoginType;
import com.iotechn.microunimall.order.api.service.biz.OrderBizService;
import com.iotechn.microunimall.order.api.service.open.OrderService;
import com.iotechn.microunimall.order.mapper.OrderMapper;
import com.iotechn.microunimall.order.mapper.OrderSkuMapper;
import com.iotechn.microunimall.show.api.domain.AppraiseDO;
import com.iotechn.microunimall.show.api.dto.AppraiseRequestDTO;
import com.iotechn.microunimall.show.api.dto.AppraiseRequestItemDTO;
import com.iotechn.microunimall.show.api.service.biz.AppraiseBizService;
import com.iotechn.microunimall.user.api.domain.AddressDO;
import com.iotechn.microunimall.user.api.domain.UserDO;
import com.iotechn.microunimall.user.api.domain.UserFormIdDO;
import com.iotechn.microunimall.user.api.enums.UserLevelType;
import com.iotechn.microunimall.user.api.service.biz.AddressBizService;
import com.iotechn.microunimall.user.api.service.biz.UserBizService;
import io.seata.spring.annotation.GlobalTransactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/11
 * Time: 20:01
 */
@Service("orderService")
@EnableConfigurationProperties(UnimallAppConfiguration.class)
public class OrderServiceImpl implements OrderService {

    private static final Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);

    private static final String TAKE_ORDER_LOCK = "TAKE_ORDER_";

    @Autowired
    private UnimallAppConfiguration appConfiguration;

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private OrderSkuMapper orderSkuMapper;

    @Autowired
    private AddressBizService addressBizService;

    @Autowired
    private CategoryBizService categoryBizService;

    @Autowired
    private WxPayService wxPayService;

    @Autowired
    private LockComponent lockComponent;

    @Autowired
    private OrderBizService orderBizService;

    @Autowired
    private FreightBizService freightBizService;

    @Autowired
    private UserBizService userBizService;

    @Autowired
    private GoodsBizService goodsBizService;

    @Autowired
    private CouponBizService couponBizService;

    @Autowired
    private CartBizService cartBizService;

    @Autowired
    private AppraiseBizService appraiseBizService;

    @Value("${com.iotechn.unimall.machine-no}")
    private String MACHINE_NO;

    @Value("${com.iotechn.unimall.env}")
    private String ENV;

    @Override
    @GlobalTransactional
    @Transactional(rollbackFor = Exception.class)
    public String takeOrder(OrderRequestDTO orderRequest, String channel, Long userId) throws ServiceException {
        if (lockComponent.tryLock(TAKE_ORDER_LOCK + userId, 20)) {
            //加上乐观锁，防止用户重复提交订单
            try {
                UserDO userDO = userBizService.getUserById(userId);
                //用户会员等级
                Integer userLevel = userDO.getLevel();
                //参数强校验 START
                List<OrderRequestSkuDTO> skuList = orderRequest.getSkuList();
                if (CollectionUtils.isEmpty(skuList) || orderRequest.getTotalPrice() == null) {
                    throw new AppServiceException(ExceptionDefinition.PARAM_CHECK_FAILED);
                }
                if (orderRequest.getTotalPrice() <= 0) {
                    throw new AppServiceException(ExceptionDefinition.ORDER_PRICE_MUST_GT_ZERO);
                }
                //商品价格
                int skuPrice = 0;
                int skuOriginalPrice = 0;
                //稍后用于优惠券作用范围校验
                Map<Long, Integer> categoryPriceMap = new HashMap<>();
                //稍后用于插入OrderSku
                Map<Long, SkuDTO> skuIdDTOMap = new HashMap<>();
                for (OrderRequestSkuDTO orderRequestSkuDTO : skuList) {
                    SkuDTO skuDTO = goodsBizService.getSkuById(orderRequestSkuDTO.getSkuId());
                    skuIdDTOMap.put(skuDTO.getId(), skuDTO);
                    if (skuDTO == null) {
                        throw new AppServiceException(ExceptionDefinition.ORDER_SKU_NOT_EXIST);
                    }
                    if (skuDTO.getStock() < orderRequestSkuDTO.getNum()) {
                        throw new AppServiceException(ExceptionDefinition.ORDER_SKU_STOCK_NOT_ENOUGH);
                    }
                    int p;
                    if (userLevel == UserLevelType.VIP.getCode()) {
                        p = skuDTO.getVipPrice() * orderRequestSkuDTO.getNum();
                    } else {
                        p = skuDTO.getPrice() * orderRequestSkuDTO.getNum();
                    }
                    skuPrice += p;
                    skuOriginalPrice += skuDTO.getOriginalPrice();
                    List<Long> categoryFamily = categoryBizService.getCategoryFamily(skuDTO.getCategoryId());
                    for (Long cid : categoryFamily) {
                        Integer price = categoryPriceMap.get(cid);
                        if (price == null) {
                            price = p;
                        } else {
                            price += p;
                        }
                        categoryPriceMap.put(cid, price);
                    }
                }

                if (skuPrice != orderRequest.getTotalPrice()) {
                    throw new AppServiceException(ExceptionDefinition.ORDER_PRICE_CHECK_FAILED);
                }

                //优惠券折扣价格
                int couponPrice = 0;
                //优惠券校验
                UserCouponDTO userCouponFromFront = orderRequest.getCoupon();
                if (userCouponFromFront != null) {
                    if (userCouponFromFront.getId() == null || userCouponFromFront.getDiscount() == null) {
                        throw new AppServiceException(ExceptionDefinition.PARAM_CHECK_FAILED);
                    }

                    UserCouponDTO userCouponFromDB = couponBizService.getUserCouponById(userCouponFromFront.getId(), userId);

                    if (userCouponFromDB == null) {
                        throw new AppServiceException(ExceptionDefinition.ORDER_COUPON_NOT_EXIST);
                    }

                    if (!userCouponFromDB.getDiscount().equals(userCouponFromFront.getDiscount())) {
                        throw new AppServiceException(ExceptionDefinition.ORDER_COUPON_DISCOUNT_CHECK_FAILED);
                    }

                    //校验优惠券策略是否满足 （类目策略）
                    Long categoryId = goodsBizService.getStrategy(GoodsStrategyUnionType.CATEGORY.getCode(), userCouponFromDB.getCouponId(), GoodsStrategyType.COUPON.getCode()).getStrategyId();
                    if (categoryId != null) {
                        Integer p = categoryPriceMap.get(categoryId);
                        if (p < userCouponFromDB.getMin()) {
                            throw new AppServiceException(ExceptionDefinition.ORDER_COUPON_PRICE_NOT_ENOUGH);
                        }
                    } else {
                        if (skuPrice < userCouponFromDB.getMin()) {
                            throw new AppServiceException(ExceptionDefinition.ORDER_COUPON_PRICE_NOT_ENOUGH);
                        }
                    }
                    couponPrice = userCouponFromDB.getDiscount();
                }

                FreightCalcRequest freightCalcRequest = new FreightCalcRequest();
                Map<Long, Integer> map = null;
                try {
                    map = orderRequest.getSkuList().stream().collect(Collectors.groupingBy(
                            item -> {
                                try {
                                    //从商品策略表查询 运费 策略
                                    SkuDTO skuDTO = skuIdDTOMap.get(item.getSkuId());
                                    GoodsStrategyDO strategy = goodsBizService.getStrategy(GoodsStrategyUnionType.SPU.getCode(), skuDTO.getSpuId(), GoodsStrategyType.FRIGHT.getCode());
                                    if (strategy.getStrategyId() == null) {
                                        throw new AppServiceException(ExceptionDefinition.ORDER_GOODS_STRATEGY_NOT_ADAPT);
                                    }
                                    return strategy.getStrategyId();
                                } catch (ServiceException e) {
                                    throw new RuntimeException();
                                }
                            }, Collectors.summingInt(subItem -> subItem.getNum())));
                } catch (Exception e) {
                    throw new AppServiceException(ExceptionDefinition.ORDER_GOODS_STRATEGY_NOT_ADAPT);
                }
                freightCalcRequest.setPrice(skuPrice - couponPrice);
                freightCalcRequest.setMap(map);
                if (orderRequest.getAddressId() != null) {
                    AddressDO addressDO = addressBizService.getAddressById(orderRequest.getAddressId());
                    freightCalcRequest.setProvince(addressDO.getProvince());
                }
                Integer freightPrice = freightBizService.getFreightMoney(freightCalcRequest);
                //参数强校验 END
                //???是否校验actualPrice??强迫校验？
                int actualPrice = skuPrice - couponPrice + freightPrice;
                Date now = new Date();
                OrderDO orderDO = new OrderDO();
                orderDO.setSkuTotalPrice(skuPrice);
                orderDO.setSkuOriginalTotalPrice(skuOriginalPrice);
                orderDO.setChannel(channel);
                orderDO.setActualPrice(actualPrice);
                if (couponPrice != 0) {
                    orderDO.setCouponId(orderRequest.getCoupon().getCouponId());
                    orderDO.setCouponPrice(couponPrice);
                }
                orderDO.setMono(orderRequest.getMono());
                orderDO.setFreightPrice(freightPrice);
                orderDO.setOrderNo(GeneratorUtil.genOrderId(MACHINE_NO, ENV));
                orderDO.setUserId(userId);
                orderDO.setStatus(OrderStatusType.UNPAY.getCode());
                orderDO.setGmtUpdate(now);
                orderDO.setGmtCreate(now);

                if (orderRequest.getAddressId() != null) {
                    AddressDO addressDO = addressBizService.getAddressById(orderRequest.getAddressId());
                    if (!userId.equals(addressDO.getUserId())) {
                        throw new AppServiceException(ExceptionDefinition.ORDER_ADDRESS_NOT_BELONGS_TO_YOU);
                    }
                    orderDO.setConsignee(addressDO.getConsignee());
                    orderDO.setPhone(addressDO.getPhone());
                    orderDO.setProvince(addressDO.getProvince());
                    orderDO.setCity(addressDO.getCity());
                    orderDO.setCounty(addressDO.getCounty());
                    orderDO.setAddress(addressDO.getAddress());
                }
                orderMapper.insert(orderDO);

                //扣除用户优惠券
                if (orderDO.getCouponId() != null) {
                    UserCouponDO updateUserCouponDO = new UserCouponDO();
                    updateUserCouponDO.setId(orderDO.getCouponId());
                    updateUserCouponDO.setGmtUsed(now);
                    updateUserCouponDO.setOrderId(orderDO.getId());
                    couponBizService.updateById(updateUserCouponDO);
                }

                //插入OrderSku
                skuList.forEach(item -> {
                    SkuDTO skuDTO = skuIdDTOMap.get(item.getSkuId());
                    OrderSkuDO orderSkuDO = new OrderSkuDO();
                    orderSkuDO.setBarCode(skuDTO.getBarCode());
                    orderSkuDO.setTitle(skuDTO.getTitle());
                    orderSkuDO.setUnit(skuDTO.getUnit());
                    orderSkuDO.setSpuTitle(skuDTO.getSpuTitle());
                    orderSkuDO.setImg(skuDTO.getImg() == null ? skuDTO.getSpuImg() : skuDTO.getImg());
                    orderSkuDO.setNum(item.getNum());
                    orderSkuDO.setOriginalPrice(skuDTO.getOriginalPrice());
                    orderSkuDO.setPrice(skuDTO.getPrice());
                    if (userLevel == UserLevelType.VIP.getCode()) {
                        orderSkuDO.setPrice(skuDTO.getVipPrice());
                    } else {
                        orderSkuDO.setPrice(skuDTO.getPrice());
                    }
                    orderSkuDO.setSkuId(skuDTO.getId());
                    orderSkuDO.setSpuId(skuDTO.getSpuId());
                    orderSkuDO.setOrderNo(orderDO.getOrderNo());
                    orderSkuDO.setOrderId(orderDO.getId());
                    orderSkuDO.setGmtCreate(now);
                    orderSkuDO.setGmtUpdate(now);
                    orderSkuMapper.insert(orderSkuDO);

                    //扣除库存
                    goodsBizService.decSkuStock(item.getSkuId(), item.getNum());
                });

                if (!StringUtils.isEmpty(orderRequest.getTakeWay())) {
                    String takeWay = orderRequest.getTakeWay();
                    if ("cart".equals(takeWay)) {
                        //扣除购物车
                        List<Long> skuIds = skuList.stream().map(item -> item.getSkuId()).collect(Collectors.toList());
                        cartBizService.deleteCartByIds(skuIds, userId);
                    }
                    //直接购买传值为 "buy"
                }

                return orderDO.getOrderNo();

            } catch (ServiceException e) {
                throw e;
            } catch (Exception e) {
                logger.error("[提交订单] 异常", e);
                throw new AppServiceException(ExceptionDefinition.ORDER_UNKNOWN_EXCEPTION);
            } finally {
                lockComponent.release(TAKE_ORDER_LOCK + userId);
            }
        }
        throw new AppServiceException(ExceptionDefinition.ORDER_SYSTEM_BUSY);
    }

    @Override
    public Page<OrderDTO> getOrderPage(Integer pageNo, Integer pageSize, Integer status, Long userId) throws ServiceException {
        List<OrderDTO> orderDTOList = orderMapper.selectOrderPage(status, (pageNo - 1) * pageSize, pageSize, userId);
        Long count = orderMapper.countOrder(status, (pageNo - 1) * pageSize, pageSize, userId);
        //封装SKU
        orderDTOList.forEach(item -> {
            item.setSkuList(orderSkuMapper.selectList(new EntityWrapper<OrderSkuDO>().eq("order_id", item.getId())));
        });
        return new Page<>(orderDTOList, pageNo, pageSize, count);
    }

    @Override
    public OrderDTO getOrderDetail(Long orderId, Long userId) throws ServiceException {
        return orderBizService.getOrderDetail(orderId, userId);
    }

    /**
     * 微信支付回调，只进行了保存FormId的远程调用，这里不适用分布式事务。
     *
     * @param orderNo
     * @param ip
     * @param userId
     * @return
     * @throws ServiceException
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object wxPrepay(String orderNo, String ip, Long userId) throws ServiceException {
        Date now = new Date();
        OrderDO orderDO = orderBizService.checkOrderExist(orderNo, userId);
        // 检测订单状态
        Integer status = orderDO.getStatus();
        if (status != OrderStatusType.UNPAY.getCode()) {
            throw new AppServiceException(ExceptionDefinition.ORDER_STATUS_NOT_SUPPORT_PAY);
        }

        UserDO userDO = userBizService.getUserById(userId);
        Integer loginType = userDO.getLoginType();
        String appId;
        String tradeType;
        if (UserLoginType.MP_WEIXIN.getCode() == loginType) {
            appId = appConfiguration.getMiniAppId();
            tradeType = WxPayConstants.TradeType.JSAPI;
        } else if (UserLoginType.APP_WEIXIN.getCode() == loginType || UserLoginType.REGISTER.getCode() == loginType) {
            appId = appConfiguration.getAppAppId();
            tradeType = WxPayConstants.TradeType.APP;
        } else if (UserLoginType.H5_WEIXIN.getCode() == loginType) {
            appId = appConfiguration.getH5AppId();
            tradeType = WxPayConstants.TradeType.JSAPI;
        } else {
            throw new AppServiceException(ExceptionDefinition.ORDER_LOGIN_TYPE_NOT_SUPPORT_WXPAY);
        }

        Object result = null;
        try {
            WxPayUnifiedOrderRequest orderRequest = new WxPayUnifiedOrderRequest();
            orderRequest.setAppid(appId);
            orderRequest.setOutTradeNo(orderNo);
            orderRequest.setOpenid(userDO.getOpenId());
            orderRequest.setBody("订单：" + orderNo);
            orderRequest.setTotalFee(orderDO.getActualPrice());
            orderRequest.setSpbillCreateIp(ip);
            orderRequest.setTradeType(tradeType);
            result = wxPayService.createOrder(orderRequest);
            if (result instanceof WxPayMpOrderResult) {
                String prepayId = ((WxPayMpOrderResult) result).getPackageValue();
                prepayId = prepayId.replace("prepay_id=", "");
                UserFormIdDO userFormIdDO = new UserFormIdDO();
                userFormIdDO.setFormId(prepayId);
                userFormIdDO.setUserId(userId);
                userFormIdDO.setOpenid(userDO.getOpenId());
                userFormIdDO.setGmtUpdate(now);
                userFormIdDO.setGmtCreate(now);
                userBizService.setValidFormId(userFormIdDO);
            }
        } catch (WxPayException e) {
            logger.error("[微信支付] 异常", e);
            throw new ThirdPartServiceException(e.getErrCodeDes(), ExceptionDefinition.THIRD_PART_SERVICE_EXCEPTION.getCode());
        } catch (Exception e) {
            logger.error("[预付款异常]", e);
            throw new AppServiceException(ExceptionDefinition.ORDER_UNKNOWN_EXCEPTION);
        }
        return result;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object offlinePrepay(String orderNo, Long userId) throws ServiceException {
        OrderDO orderDO = orderBizService.checkOrderExist(orderNo, userId);
        // 检测订单状态
        Integer status = orderDO.getStatus();
        if (status != OrderStatusType.UNPAY.getCode()) {
            throw new AppServiceException(ExceptionDefinition.ORDER_STATUS_NOT_SUPPORT_PAY);
        }
        OrderDO updateOrderDO = new OrderDO();
        updateOrderDO.setPayChannel(PayChannelType.OFFLINE.getCode());
        updateOrderDO.setStatus(OrderStatusType.WAIT_STOCK.getCode());
        updateOrderDO.setGmtUpdate(new Date());
        boolean succ = orderBizService.changeOrderStatus(orderNo, OrderStatusType.UNPAY.getCode(), updateOrderDO);
        if (succ) {
            return "ok";
        }
        throw new AppServiceException(ExceptionDefinition.ORDER_STATUS_CHANGE_FAILED);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String refund(String orderNo, Long userId) throws ServiceException {
        OrderDO orderDO = orderBizService.checkOrderExist(orderNo, userId);
        if (PayChannelType.OFFLINE.getCode().equals(orderDO.getPayChannel())) {
            throw new AppServiceException(ExceptionDefinition.ORDER_PAY_CHANNEL_NOT_SUPPORT_REFUND);
        }
        if (OrderStatusType.refundable(orderDO.getStatus())) {
            OrderDO updateOrderDO = new OrderDO();
            updateOrderDO.setStatus(OrderStatusType.REFUNDING.getCode());
            orderBizService.changeOrderStatus(orderNo, orderDO.getStatus(), updateOrderDO);
            return "ok";
        }
        throw new AppServiceException(ExceptionDefinition.ORDER_STATUS_NOT_SUPPORT_REFUND);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String cancel(String orderNo, Long userId) throws ServiceException {
        OrderDO orderDO = orderBizService.checkOrderExist(orderNo, userId);
        if (orderDO.getStatus() != OrderStatusType.UNPAY.getCode()) {
            throw new AppServiceException(ExceptionDefinition.ORDER_STATUS_NOT_SUPPORT_CANCEL);
        }
        OrderDO updateOrderDO = new OrderDO();
        updateOrderDO.setStatus(OrderStatusType.CANCELED.getCode());
        updateOrderDO.setGmtUpdate(new Date());
        orderBizService.changeOrderStatus(orderNo, OrderStatusType.UNPAY.getCode(), updateOrderDO);
        return "ok";
    }

    @Override
    public String confirm(String orderNo, Long userId) throws ServiceException {
        OrderDO orderDO = orderBizService.checkOrderExist(orderNo, userId);
        if (orderDO.getStatus() != OrderStatusType.WAIT_CONFIRM.getCode()) {
            throw new AppServiceException(ExceptionDefinition.ORDER_STATUS_NOT_SUPPORT_CONFIRM);
        }
        OrderDO updateOrderDO = new OrderDO();
        updateOrderDO.setStatus(OrderStatusType.WAIT_APPRAISE.getCode());
        updateOrderDO.setGmtUpdate(new Date());
        orderBizService.changeOrderStatus(orderNo, OrderStatusType.WAIT_CONFIRM.getCode(), updateOrderDO);
        return "ok";
    }

    @Override
    public ShipTraceDTO queryShip(String orderNo, Long userId) throws ServiceException {
        OrderDO orderDO = orderBizService.checkOrderExist(orderNo, userId);
        if (orderDO.getStatus() < OrderStatusType.WAIT_CONFIRM.getCode()) {
            throw new AppServiceException(ExceptionDefinition.ORDER_HAS_NOT_SHIP);
        }
        if (StringUtils.isEmpty(orderDO.getShipCode()) || StringUtils.isEmpty(orderDO.getShipNo())) {
            throw new AppServiceException(ExceptionDefinition.ORDER_DID_NOT_SET_SHIP);
        }
        ShipTraceDTO shipTraceList = freightBizService.getShipTraceList(orderDO.getShipNo(), orderDO.getShipCode());
        if (CollectionUtils.isEmpty(shipTraceList.getTraces())) {
            throw new AppServiceException(ExceptionDefinition.ORDER_DO_NOT_EXIST_SHIP_TRACE);
        }
        return shipTraceList;
    }

    @Override
    public Integer preview(Long userId, OrderRequestDTO orderRequest) throws ServiceException {
        UserDO userDO = userBizService.getUserById(userId);
        //用户会员等级
        Integer userLevel = userDO.getLevel();
        //参数强校验 START
        List<OrderRequestSkuDTO> skuList = orderRequest.getSkuList();
        if (CollectionUtils.isEmpty(skuList) || orderRequest.getTotalPrice() == null) {
            throw new AppServiceException(ExceptionDefinition.PARAM_CHECK_FAILED);
        }
        if (orderRequest.getTotalPrice() <= 0) {
            throw new AppServiceException(ExceptionDefinition.ORDER_PRICE_MUST_GT_ZERO);
        }
        //商品价格
        int skuPrice = 0;
        //稍后用于插入OrderSku
        Map<Long, SkuDTO> skuIdDTOMap = new HashMap<>();
        for (OrderRequestSkuDTO orderRequestSkuDTO : skuList) {
            SkuDTO skuDTO = goodsBizService.getSkuById(orderRequestSkuDTO.getSkuId());
            skuIdDTOMap.put(skuDTO.getId(), skuDTO);
            int p;
            if (userLevel == UserLevelType.VIP.getCode()) {
                p = skuDTO.getVipPrice() * orderRequestSkuDTO.getNum();
            } else {
                p = skuDTO.getPrice() * orderRequestSkuDTO.getNum();
            }
            skuPrice += p;
        }

        //优惠券校验
        int couponPrice = orderRequest.getCoupon().getDiscount();
        FreightCalcRequest freightCalcRequest = new FreightCalcRequest();
        Map<Long, Integer> map = null;
        try {
            map = orderRequest.getSkuList().stream().collect(Collectors.groupingBy(
                    item -> {
                        try {
                            //从商品策略表查询 运费 策略
                            SkuDTO skuDTO = skuIdDTOMap.get(item.getSkuId());
                            GoodsStrategyDO strategy = goodsBizService.getStrategy(GoodsStrategyUnionType.SPU.getCode(), skuDTO.getSpuId(), GoodsStrategyType.FRIGHT.getCode());
                            if (strategy.getStrategyId() == null) {
                                throw new AppServiceException(ExceptionDefinition.ORDER_GOODS_STRATEGY_NOT_ADAPT);
                            }
                            return strategy.getStrategyId();
                        } catch (ServiceException e) {
                            throw new RuntimeException();
                        }
                    }, Collectors.summingInt(subItem -> subItem.getNum())));
        } catch (Exception e) {
            throw new AppServiceException(ExceptionDefinition.ORDER_GOODS_STRATEGY_NOT_ADAPT);
        }
        freightCalcRequest.setPrice(skuPrice - couponPrice);
        freightCalcRequest.setMap(map);
        if (orderRequest.getAddressId() != null) {
            AddressDO addressDO = addressBizService.getAddressById(orderRequest.getAddressId());
            freightCalcRequest.setProvince(addressDO.getProvince());
        }
        Integer freightPrice = freightBizService.getFreightMoney(freightCalcRequest);
        return freightPrice;
    }

    @Override
    @GlobalTransactional
    @Transactional(rollbackFor = Exception.class)
    public Boolean addAppraise(AppraiseRequestDTO appraiseRequestDTO, Long userId) throws ServiceException {
        if (appraiseRequestDTO.getOrderId() == null) {
            throw new AppServiceException(ExceptionDefinition.APPRAISE_PARAM_CHECK_FAILED);
        }
        //校验是否有对应等待评价的订单
        Integer integer = orderMapper.selectCount(
                new EntityWrapper<OrderDO>()
                        .eq("id", appraiseRequestDTO.getOrderId())
                        .eq("status", OrderStatusType.WAIT_APPRAISE.getCode())
                        .eq("user_id", userId));
        if (integer == 0) {
            throw new AppServiceException(ExceptionDefinition.APPRAISE_ORDER_CHECK_FAILED);
        }
        //如果传入评价list中没有数据，就直接转变订单状态发出
        Date now = new Date();
        if (CollectionUtils.isEmpty(appraiseRequestDTO.getAppraiseDTOList())) {
            OrderDO orderDO = new OrderDO();
            orderDO.setStatus(OrderStatusType.COMPLETE.getCode());
            orderDO.setId(appraiseRequestDTO.getOrderId());
            orderDO.setGmtUpdate(now);
            orderMapper.updateById(orderDO);
        }

        //校验是否是该订单中的商品
        for (AppraiseRequestItemDTO itemDTO : appraiseRequestDTO.getAppraiseDTOList()) {
            List<OrderSkuDO> skuDOList = orderSkuMapper.selectList(new EntityWrapper<OrderSkuDO>()
                    .eq("order_id", appraiseRequestDTO.getOrderId())
                    .eq("spu_id", itemDTO.getSpuId())
                    .eq("sku_id", itemDTO.getSkuId()));
            //从order_sku表中 验证是否有对应的表单和商品
            if (CollectionUtils.isEmpty(skuDOList)) {
                throw new AppServiceException(ExceptionDefinition.APPRAISE_PARAM_CHECK_FAILED);
            }
            itemDTO.setSkuTitle(skuDOList.get(0).getTitle());
        }

        appraiseBizService.addAppraise(appraiseRequestDTO);

        //改变订单状态
        OrderDO orderDO = new OrderDO();
        orderDO.setStatus(OrderStatusType.COMPLETE.getCode());
        orderDO.setId(appraiseRequestDTO.getOrderId());
        orderDO.setGmtUpdate(now);
        orderMapper.updateById(orderDO);
        return true;
    }

}
