package com.iotechn.microunimall.data.starter.annotaion;

import com.iotechn.microunimall.data.starter.config.db.MybatisPlusConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/8
 * Time: 10:36
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Import({MybatisPlusConfiguration.class})
@Documented
public @interface EnableUnimallDefaultMysql {
}
