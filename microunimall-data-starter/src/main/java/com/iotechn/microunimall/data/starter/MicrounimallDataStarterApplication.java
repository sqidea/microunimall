package com.iotechn.microunimall.data.starter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
public class MicrounimallDataStarterApplication {

    public static void main(String[] args) {
        SpringApplication.run(MicrounimallDataStarterApplication.class, args);
    }

}
