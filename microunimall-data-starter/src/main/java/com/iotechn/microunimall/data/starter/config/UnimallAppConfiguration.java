package com.iotechn.microunimall.data.starter.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/9
 * Time: 11:54
 */
@Data
@ConfigurationProperties(prefix = "com.iotechn.unimall.wx")
public class UnimallAppConfiguration {

    private String miniAppId;

    private String miniAppSecret;

    private String appAppId;

    private String appAppSecret;

    private String h5AppId;

    private String h5AppSecret;

}
