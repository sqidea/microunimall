package com.iotechn.microunimall.ad.api.dto;

import com.iotechn.microunimall.shopping.api.dto.RecommendDTO;
import com.iotechn.microunimall.shopping.api.dto.SpuDTO;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 首页聚合接口DTO
 * Created by rize on 2019/7/14.
 */
@Data
public class IntegralIndexDataDTO implements Serializable {

    private Map<String, List<AdvertisementDTO>> advertisement;

    private List<RecommendDTO> windowRecommend;

    private List<SpuDTO> salesTop;

    private List<SpuDTO> newTop;

}
