package com.iotechn.microunimall.ad.api.service.open;


import com.iotechn.microunimall.ad.api.domain.AdvertisementDO;
import com.iotechn.microunimall.core.annotation.HttpMethod;
import com.iotechn.microunimall.core.annotation.HttpOpenApi;
import com.iotechn.microunimall.core.annotation.HttpParam;
import com.iotechn.microunimall.core.annotation.HttpParamType;
import com.iotechn.microunimall.core.exception.ServiceException;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: kbq
 * Date: 2019-07-08
 * Time: 下午8:22
 */
@HttpOpenApi(group = "advertisement", description = "广告推销")
public interface AdvertisementService {

    @HttpMethod(description = "取得活跃广告")
    public List<AdvertisementDO> getActiveAd(
            @HttpParam(name = "adType", type = HttpParamType.COMMON, description = "广告类型") Integer adType) throws ServiceException;

}
