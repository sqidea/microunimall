package com.iotechn.microunimall.user.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.iotechn.microunimall.user.api.domain.UserFormIdDO;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/10/29
 * Time: 23:19
 */
public interface UserFormIdMapper extends BaseMapper<UserFormIdDO> {
}
