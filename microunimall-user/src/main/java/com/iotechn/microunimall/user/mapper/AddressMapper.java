package com.iotechn.microunimall.user.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.iotechn.microunimall.user.api.domain.AddressDO;

/*
@author kbq
@date  2019/7/4 - 22:09
*/
public interface AddressMapper extends BaseMapper<AddressDO> {
}
