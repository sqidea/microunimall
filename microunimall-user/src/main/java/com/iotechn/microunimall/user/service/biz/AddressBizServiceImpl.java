package com.iotechn.microunimall.user.service.biz;

import com.iotechn.microunimall.user.api.domain.AddressDO;
import com.iotechn.microunimall.user.api.service.biz.AddressBizService;
import com.iotechn.microunimall.user.mapper.AddressMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/11
 * Time: 19:08
 */
@Service("addressBizService")
public class AddressBizServiceImpl implements AddressBizService {

    @Autowired
    private AddressMapper addressMapper;

    @Override
    public AddressDO getAddressById(Long id) {
        return addressMapper.selectById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String create() {
        AddressDO addressDO = new AddressDO();
        addressDO.setProvince("p");
        addressDO.setCity("city");
        addressDO.setCounty("county");
        addressDO.setAddress("address");
        addressDO.setDefaultAddress(0);
        addressDO.setUserId(0l);
        addressDO.setPhone("185555555555");
        addressDO.setConsignee("rize");
        addressDO.setGmtCreate(new Date());
        addressDO.setGmtUpdate(new Date());
        addressMapper.insert(addressDO);
        return "ok";
    }

}
