package com.iotechn.microunimall.user.service.biz;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.iotechn.microunimall.core.Const;
import com.iotechn.microunimall.core.exception.ServiceException;
import com.iotechn.microunimall.data.starter.compent.CacheComponent;
import com.iotechn.microunimall.data.starter.config.UnimallAppConfiguration;
import com.iotechn.microunimall.user.api.domain.UserDO;
import com.iotechn.microunimall.user.api.domain.UserFormIdDO;
import com.iotechn.microunimall.user.api.dto.UserDTO;
import com.iotechn.microunimall.user.api.model.WeChatCommonTemplateMessageModel;
import com.iotechn.microunimall.user.api.service.biz.UserBizService;
import com.iotechn.microunimall.user.api.exception.UserExceptionDefinition;
import com.iotechn.microunimall.user.api.exception.UserServiceException;
import com.iotechn.microunimall.user.mapper.UserFormIdMapper;
import com.iotechn.microunimall.user.mapper.UserMapper;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/7
 * Time: 23:44
 */
@Service("userBizService")
@EnableConfigurationProperties(UnimallAppConfiguration.class)
public class UserBizServiceImpl implements UserBizService {
    private OkHttpClient okHttpClient = new OkHttpClient();

    private static final String CA_OFFICIAL_WECHAT_ACCESS = "CA_OFFICIAL_WECHAT_ACCESS";

    private static final String CA_OFFICIAL_WECHAT_TICKET = "CA_OFFICIAL_WECHAT_TICKET";

    private static final String CA_MINI_WECHAT_ACCESS = "CA_MINI_WECHAT_ACCESS";

    @Autowired
    private UnimallAppConfiguration appConfiguration;

    @Autowired
    private CacheComponent cacheComponent;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserFormIdMapper userFormIdMapper;

    @Autowired
    private StringRedisTemplate userRedisTemplate;

    private static final Logger logger = LoggerFactory.getLogger(UserBizService.class);

    @Override
    public String getWxH5AccessToken() throws Exception {
        String wxAccessToken = cacheComponent.getRaw(CA_OFFICIAL_WECHAT_ACCESS);
        if (StringUtils.isEmpty(wxAccessToken)) {
            //尝试获取微信公众号Token
            String accessJson = okHttpClient.newCall(
                    new Request.Builder()
                            .url("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appConfiguration.getH5AppId() + "&secret=" + appConfiguration.getH5AppSecret())
                            .get()
                            .build()).execute().body().string();
            JSONObject jsonObject = JSONObject.parseObject(accessJson);
            wxAccessToken = jsonObject.getString("access_token");
            if (!StringUtils.isEmpty(wxAccessToken)) {
                Integer expires_in = jsonObject.getInteger("expires_in");
                //在过期前重置
                Integer cacheExpireSec = expires_in * 4 / 5;
                cacheComponent.putRaw(CA_OFFICIAL_WECHAT_ACCESS, wxAccessToken, cacheExpireSec);
            } else {
                throw new RuntimeException("回复错误:" + accessJson);
            }
        }
        return wxAccessToken;
    }

    @Override
    public String getWxH5Ticket(String accessToken) throws Exception {
        String wxTicket = cacheComponent.getRaw(CA_OFFICIAL_WECHAT_TICKET);
        if (StringUtils.isEmpty(wxTicket)) {
            //尝试获取微信公众号Ticket
            String ticketJson = okHttpClient.newCall(
                    new Request.Builder()
                            .url("https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=" + accessToken + "&type=jsapi")
                            .get()
                            .build()).execute().body().string();
            JSONObject jsonObject = JSONObject.parseObject(ticketJson);
            wxTicket = jsonObject.getString("ticket");
            if (!StringUtils.isEmpty(wxTicket)) {
                Integer expires_in = jsonObject.getInteger("expires_in");
                //在过期前重置
                Integer cacheExpireSec = expires_in * 4 / 5;
                cacheComponent.putRaw(CA_OFFICIAL_WECHAT_TICKET, wxTicket, cacheExpireSec);
            } else {
                throw new RuntimeException("回复错误:" + ticketJson);
            }
        }
        return wxTicket;
    }

    @Override
    public String getWxMiniAccessToken() throws Exception {
        String access_token = cacheComponent.getRaw(CA_MINI_WECHAT_ACCESS);
        if (StringUtils.isEmpty(access_token)) {
            String accessJson = okHttpClient.newCall(
                    new Request.Builder()
                            .url("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appConfiguration.getMiniAppId() + "&secret=" + appConfiguration.getMiniAppSecret())
                            .get()
                            .build()).execute().body().string();
            JSONObject jsonObject = JSONObject.parseObject(accessJson);
            access_token = jsonObject.getString("access_token");
            if (!StringUtils.isEmpty(access_token)) {
                Integer expires_in = jsonObject.getInteger("expires_in");
                Integer cacheExpireSec = expires_in * 4 / 5;
                cacheComponent.putRaw(CA_MINI_WECHAT_ACCESS, access_token, cacheExpireSec);
            } else {
                throw new RuntimeException("回复错误:" + accessJson);
            }
        }
        return access_token;
    }

    @Override
    public boolean sendWechatMiniTemplateMessage(WeChatCommonTemplateMessageModel model) {
        try {
            //step1. accessToken
            String access_token = this.getWxMiniAccessToken();
            //step2. 发送请求
            int i = wechatCommonTemplateMessage(model, "https://api.weixin.qq.com/cgi-bin/message/wxopen/template/send?access_token=" + access_token);
            return i == 0;
        } catch (Exception e) {
            logger.error("[微信模版消息] 异常", e);
        }
        return false;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void setValidFormId(UserFormIdDO userFormIdDO) {
        if (!userFormIdDO.getFormId().contains("mock")) {
            if (!StringUtils.isEmpty(userFormIdDO.getOpenid())) {
                userFormIdMapper.insert(userFormIdDO);
            } else {
                logger.info("[传入openid为空]" + JSONObject.toJSONString(userFormIdDO));
            }
        }
    }

    @Override
    public UserFormIdDO getValidFormIdByUserId(Long userId) {
        List<UserFormIdDO> userFormDOS = userFormIdMapper.selectList(
                new EntityWrapper<UserFormIdDO>()
                        .eq("user_id", userId)
                        .gt("gmt_create", new Date(System.currentTimeMillis() - (1000l * 60 * 60 * 24 * 7 - 1000l * 60 * 30))));
        if (CollectionUtils.isEmpty(userFormDOS)) {
            return null;
        }
        UserFormIdDO userFormDO = userFormDOS.get(0);
        userFormIdMapper.deleteById(userFormDO.getId());
        return userFormDO;
    }

    @Override
    public UserDTO getUserDTOByAccessToken(String accessToken) throws ServiceException {
        String userJson = userRedisTemplate.opsForValue().get(Const.USER_REDIS_PREFIX + accessToken);
        if (!StringUtils.isEmpty(userJson)) {
            UserDTO userDTO = JSONObject.parseObject(userJson, UserDTO.class);
            userRedisTemplate.expire(Const.USER_REDIS_PREFIX + accessToken, 30, TimeUnit.MINUTES);
            return userDTO;
        }
        throw new UserServiceException(UserExceptionDefinition.USER_NOT_LOGIN);
    }

    @Override
    public UserDO getUserById(Long userId) throws ServiceException {
        return userMapper.selectById(userId);
    }

    /**
     * 抽取 小程序模板消息 公众号模板消息共同代码
     *
     * @param model
     * @param url
     * @throws Exception
     */
    private int wechatCommonTemplateMessage(WeChatCommonTemplateMessageModel model, String url) throws Exception {
        String modelJson = JSONObject.toJSONString(model);
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, modelJson);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        String res = okHttpClient.newCall(request).execute().body().string();
        JSONObject jsonObject = JSONObject.parseObject(res);
        Integer errcode = jsonObject.getInteger("errcode");
        if (errcode != 0) {
            logger.error("[模板消息回复] 错误，请求报文=" + modelJson);
            logger.error("[模板消息回复] 错误，回复报文=" + res);
        }
        return errcode;
    }

}
