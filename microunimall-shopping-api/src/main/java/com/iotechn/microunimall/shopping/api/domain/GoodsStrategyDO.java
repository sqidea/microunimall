package com.iotechn.microunimall.shopping.api.domain;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/18
 * Time: 11:09
 */
@Data
@TableName("unimall_goods_strategy")
public class GoodsStrategyDO extends SuperDO implements Serializable {

    private Long id;

    @TableField("union_type")
    private Integer unionType;

    @TableField("union_id")
    private Long unionId;

    @TableField("strategy_type")
    private Integer strategyType;

    @TableField("strategy_id")
    private Long strategyId;

}
