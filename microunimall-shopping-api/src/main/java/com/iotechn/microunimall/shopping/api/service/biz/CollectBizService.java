package com.iotechn.microunimall.shopping.api.service.biz;

import com.iotechn.microunimall.core.exception.ServiceException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/10
 * Time: 16:13
 */
public interface CollectBizService {

    public Boolean getCollectBySpuId(Long spuId, Long userId) throws ServiceException;

}
