package com.iotechn.microunimall.shopping.api.domain;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by rize on 2019/7/3.
 */
@Data
@TableName("unimall_cart")
public class CartDO extends SuperDO implements Serializable {

    @TableField("sku_id")
    private Long skuId;

    @TableField("user_id")
    private Long userId;

    private Integer num;

}
