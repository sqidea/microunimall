package com.iotechn.microunimall.shopping.api.service.biz;

import com.iotechn.microunimall.core.exception.ServiceException;
import com.iotechn.microunimall.core.model.Page;
import com.iotechn.microunimall.shopping.api.domain.GoodsStrategyDO;
import com.iotechn.microunimall.shopping.api.domain.SpuDO;
import com.iotechn.microunimall.shopping.api.dto.SkuDTO;
import com.iotechn.microunimall.shopping.api.dto.SpuDTO;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/10
 * Time: 14:23
 */
public interface GoodsBizService {

    public Page<SpuDTO> getGoodsPage(Integer pageNo, Integer pageSize, Long categoryId, String orderBy, Boolean isAsc, String title) throws ServiceException;

    /**
     * 通过Id获取SpuDO 领域对象
     * @param spuId
     * @return
     * @throws ServiceException
     */
    public SpuDO getSpuById(Long spuId) throws ServiceException;

    public SpuDTO getGoods(Long spuId, Long userId) throws ServiceException;

    public void clearGoodsCache(Long spuId);

    public SkuDTO getSkuById(Long skuId);

    public Integer decSkuStock(Long skuId, Integer num);

    public GoodsStrategyDO getStrategy(Integer unionType, Long unionId, Integer strategyType) throws ServiceException;

}
