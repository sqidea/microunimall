package com.iotechn.microunimall.shopping.api.service.open;

import com.iotechn.microunimall.core.annotation.HttpMethod;
import com.iotechn.microunimall.core.annotation.HttpOpenApi;
import com.iotechn.microunimall.core.annotation.HttpParam;
import com.iotechn.microunimall.core.annotation.HttpParamType;
import com.iotechn.microunimall.core.annotation.param.NotNull;
import com.iotechn.microunimall.core.exception.ServiceException;
import com.iotechn.microunimall.shopping.api.dto.CategoryDTO;

import java.util.List;

/**
 * Created by rize on 2019/7/2.
 */
@HttpOpenApi(group = "category", description = "类目服务")
public interface CategoryService {

    @HttpMethod(description = "获取类目列表")
    public List<CategoryDTO> categoryList() throws ServiceException;

    @HttpMethod(description = "获取分类父节点")
    public List<Long> getCategoryFamily(
            @NotNull @HttpParam(name = "categoryId", type = HttpParamType.COMMON, description = "类目Id") Long categoryId) throws ServiceException;

}
