package com.iotechn.microunimall.shopping.api.service.biz;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/13
 * Time: 14:20
 */
public interface CartBizService {

    public Integer deleteCartByIds(List<Long> ids, Long userId);

}
