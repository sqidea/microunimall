package com.iotechn.microunimall.shopping.service.open;

import com.iotechn.microunimall.core.exception.ServiceException;
import com.iotechn.microunimall.shopping.api.dto.CategoryDTO;
import com.iotechn.microunimall.shopping.api.service.biz.CategoryBizService;
import com.iotechn.microunimall.shopping.api.service.open.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/11
 * Time: 21:22
 */
@Service("categoryService")
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryBizService categoryBizService;

    @Override
    public List<CategoryDTO> categoryList() throws ServiceException {
        return categoryBizService.categoryList();
    }

    /**
     * @param categoryId
     * @return
     * @throws ServiceException
     */
    @Override
    public List<Long> getCategoryFamily(Long categoryId) throws ServiceException {
        return categoryBizService.getCategoryFamily(categoryId);
    }

}
