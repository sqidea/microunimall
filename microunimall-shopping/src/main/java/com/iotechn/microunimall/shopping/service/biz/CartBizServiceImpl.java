package com.iotechn.microunimall.shopping.service.biz;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.iotechn.microunimall.shopping.api.domain.CartDO;
import com.iotechn.microunimall.shopping.api.service.biz.CartBizService;
import com.iotechn.microunimall.shopping.mapper.CartMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/13
 * Time: 14:22
 */
@Service("cartBizService")
public class CartBizServiceImpl implements CartBizService {

    @Autowired
    private CartMapper cartMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer deleteCartByIds(List<Long> ids, Long userId) {
        return cartMapper.delete(new EntityWrapper<CartDO>().in("sku_id", ids).eq("user_id", userId));
    }

}
