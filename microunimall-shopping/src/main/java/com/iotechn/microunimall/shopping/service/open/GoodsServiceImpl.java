package com.iotechn.microunimall.shopping.service.open;

import com.iotechn.microunimall.core.exception.ServiceException;
import com.iotechn.microunimall.core.model.Page;
import com.iotechn.microunimall.shopping.api.dto.SpuDTO;
import com.iotechn.microunimall.shopping.api.service.biz.GoodsBizService;
import com.iotechn.microunimall.shopping.api.service.open.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: rize
 * Date: 2019/11/10
 * Time: 14:21
 */
@Service("goodsService")
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsBizService goodsBizService;

    @Override
    public Page<SpuDTO> getGoodsPage(Integer pageNo, Integer pageSize, Long categoryId, String orderBy, Boolean isAsc, String title) throws ServiceException {
        return goodsBizService.getGoodsPage(pageNo, pageSize, categoryId, orderBy, isAsc, title);
    }

    @Override
    public SpuDTO getGoods(Long spuId, Long userId) throws ServiceException {
        return goodsBizService.getGoods(spuId, userId);
    }

}
