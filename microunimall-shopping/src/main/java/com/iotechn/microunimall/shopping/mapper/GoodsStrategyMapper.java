package com.iotechn.microunimall.shopping.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.iotechn.microunimall.shopping.api.domain.GoodsStrategyDO;

/**
 * Created by rize on 2019/7/2.
 */
public interface GoodsStrategyMapper extends BaseMapper<GoodsStrategyDO> {

}
