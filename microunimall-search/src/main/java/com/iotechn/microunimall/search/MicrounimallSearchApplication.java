package com.iotechn.microunimall.search;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicrounimallSearchApplication {

    public static void main(String[] args) {
        SpringApplication.run(MicrounimallSearchApplication.class, args);
    }

}
